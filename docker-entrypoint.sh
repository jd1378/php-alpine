#!/bin/sh

set -e

if /usr/bin/find "/docker-entrypoint.d/" -mindepth 1 -print -quit 2>/dev/null | /bin/grep -q .; then
    echo "$0: /docker-entrypoint.d/ is not empty"

    echo "$0: Looking for shell scripts in /docker-entrypoint.d/..."
    for f in $(/usr/bin/find /docker-entrypoint.d/ \( -type l -o -type f \) -name "*.sh"); do
        echo "$0: Launching $f";
        "$f"
    done
else
    echo "$0: /docker-entrypoint.d/ is empty, skipping initial configuration..."
fi

exec "$@"
