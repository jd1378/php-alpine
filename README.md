# php-alpine

a minimal alpine image with php (8.1 currently) and extensions usually needed to run laravel workers

## building locally

```sh
docker build --tag javadmnjd/php-alpine .
```

## Notes

check Dockerfile or use `php -m` to see which modules are installed.
laravel workers should work out of box.
`www-data` user is created and added to the group with the same name for you.

### IMPORTANT

Don't use below examples as is, they don't take care of caching, environment variables etc. Make sure you know what you are doing.

## How to use

Example:

```Dockerfile

FROM javadmnjd/php-alpine:php8.1-alpine3.16

RUN set -ex \
    && apk add --no-cache --update \
    php81-gd php81-iconv php81-zip \
    php81-simplexml php81-xmlreader php81-xmlwriter \
    php81-redis

COPY --from=composer:2.4 /usr/bin/composer /usr/bin/

COPY . /var/www/

WORKDIR /var/www/

RUN composer install --optimize-autoloader --no-dev \
    && php artisan config:cache \
    && mkdir -p /var/www/storage/logs/ \
    && chown -R www-data:www-data /var/www \
    && rm -rf composer* \
    && rm -rf /usr/bin/composer

ENTRYPOINT ["/usr/local/bin/php", "artisan", "queue:work", "redis"]

```
