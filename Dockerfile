ARG php_version='<php_version>'
ARG alpine_version='<alpine_version>'

FROM alpine:$alpine_version

ARG php_version

COPY docker-entrypoint.sh /usr/local/bin/

RUN set -ex \
    && mkdir -p /docker-entrypoint.d/ \
    && export php_pkg_ver=$(echo $php_version | sed 's/\.//') \
    && apk add --no-cache --update \
        tini ca-certificates curl pcre2 musl openssl php$php_pkg_ver php$php_pkg_ver-embed \
        php$php_pkg_ver-session php$php_pkg_ver-phar php$php_pkg_ver-dom \
        php$php_pkg_ver-curl \
        php$php_pkg_ver-bcmath php$php_pkg_ver-ctype php$php_pkg_ver-fileinfo \
        php$php_pkg_ver-json php$php_pkg_ver-mbstring php$php_pkg_ver-openssl \
        php$php_pkg_ver-pdo php$php_pkg_ver-pdo_mysql php$php_pkg_ver-tokenizer php$php_pkg_ver-xml \
        php$php_pkg_ver-gd php$php_pkg_ver-iconv php$php_pkg_ver-zip php$php_pkg_ver-zlib \
        php$php_pkg_ver-simplexml php$php_pkg_ver-xmlreader php$php_pkg_ver-xmlwriter \
        php$php_pkg_ver-redis php$php_pkg_ver-opcache \
    && (ln -s /usr/bin/php$php_pkg_ver /usr/bin/php || true) \
    && (ln -s /usr/bin/phar$php_pkg_ver /usr/bin/phar || true) \
    && (ln -s /usr/bin/phar.phar$php_pkg_ver /usr/bin/phar.phar || true) \
    && (ln -s /etc/php$php_pkg_ver /etc/php || true) \
    && mkdir -p /usr/local/etc/ \
    && (ln -s /etc/php$php_pkg_ver /usr/local/etc/php || true) \
    && (addgroup --system www-data || true) \
    && adduser \
      --system \
      --disabled-password \
      --ingroup www-data \
      --no-create-home \
      --home /nonexistent \
      --gecos "www-data user" \
      --shell /bin/false \
      www-data

STOPSIGNAL SIGTERM

ENTRYPOINT ["/sbin/tini", "--", "/usr/local/bin/docker-entrypoint.sh"]

CMD ["/usr/bin/php", "-a"]
